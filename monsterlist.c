#include "monster.h"

int queue_init(monster_list_t *e){
  e->size = 0;
  e->head = e->tail = NULL;
  return 0;
}
int queue_delete(monster_list_t *e){
  monster m;
  while(!queue_remove(e,&m))
   ;
  return 0;
}

int queue_add(monster_list_t *e,monster_node_t ** mn, monster m){
   if(e->head){
      e->tail->next = malloc(sizeof (*e->tail->next));
      *mn = e->tail->next;
      e->tail->prev = e->tail;
      e->tail = e->tail->next;
      e->tail->data = m;
      e->tail->next = NULL;
      e->size++;    
  }
  else{
    e->tail = e->head = malloc(sizeof (*e->tail->next));
    *mn=e->tail;
    e->tail->prev = e->head;
    e->tail->data = m;
    e->tail->next = NULL;
    e->size++;
  }
  
  
  return 0;
}


monster queue_at(monster_node_t * at){
  monster m;
  if(!at){
    
    return at->data;
  }
  return m;
}

int queue_remove(monster_list_t *e, monster *m){
    monster_node_t *n;
    
    if(!e->size){
      return 1;
    }
    n=e->head;
    e->head = e->head->next;
    //e->head->prev = NULL;
    e->size--;
    *m = n->data;
    free(n);
    if(!e->size){
      e->tail = NULL;
    }
  return 0;
}

int queue_size(monster_list_t *e){
  
  return e->size;
}


