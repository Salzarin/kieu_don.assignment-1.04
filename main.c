#include "dungeon.h"


int main(int argc, char * argv[]){
  
  //Initialize Time for Rand
  
  struct timespec ti;
  //time_t ti;
  clock_gettime(CLOCK_REALTIME, &ti);

  //Seed the RNG
  srand((unsigned) time(&ti.tv_sec));
  
  int i;
  bool save = false;
  bool load = false;
  bool player_set = false;
  bool verbose_flag = false;
  bool player_auto = false;
  
  int Max_Rooms = 5;
  int rockhardnessmap[MAX_H][MAX_V];
  memset(rockhardnessmap,0,MAX_V*MAX_H*sizeof(int));
  char * filename = malloc(50); //Guarantee Memory Space
  strcpy(filename,"dungeon");
  player p;
  
  int nummon = 5;
  int p_speed = 10;
  
  
  for(i = 1;i<argc;i++){
    if(!strcmp(argv[i],"--save")){
      save = true;
    }
    else if(!strcmp(argv[i],"--load")){
      load = true;
    }
    else if(!strcmp(argv[i],"-s")){
      if(i+1<argc){
	Max_Rooms = atoi(argv[i+1]) > 5 ? atoi(argv[i+1]) : 5;
      } 
    }    
    else if(!strcmp(argv[i], "-l")){
      if(i+1<argc){
	//filename = argv[i+1];
	if(filename[0] != '-' && strcmp(argv[i+1],"")){ 
	  //filename = argv[i+1];
	  memset(filename,'\0',50);
	  strcpy(filename,argv[i+1]);
	}
	else{
	  printf("Invalid Load File, using dungeon\n");
	  memset(filename,'\0',50);
	  strcpy(filename,"dungeon");
	}
      }   
    }
    else if(!strcmp(argv[i], "-p")){
      if(i+2<argc){
	p.x = atoi(argv[i+1]);
	p.y = atoi(argv[i+2]);
	player_set = true;
      }   
    }
    else if(!strcmp(argv[i], "-nummon")){
      if(i+1<argc){
	nummon = atoi(argv[i+1]);
      }
    }
    else if(!strcmp(argv[i], "-pspeed")){
      if(i+1<argc){
	p_speed = atoi(argv[i+1]);
      }
    }
    else if(!strcmp(argv[i], "-v")){
      verbose_flag = true;
    }
    else if(!strcmp(argv[i], "-auto")){
      player_auto = true;
    }
  }
  
  room Rooms[5000];
  //monster monsters[5000];
  monster_node_t ** monsters;
  
  
  //Initialize Dungeon Map
  int dungeonmap[MAX_H][MAX_V];
  memset(dungeonmap,0,sizeof(dungeonmap));
  nummon=nummon>0?nummon:1;
  p_speed=p_speed>0?p_speed:10;
  
  
   
  
if(!load){
  printf("Creating Dungeon\n");
  Max_Rooms = createDungeon(dungeonmap,Max_Rooms,Rooms,rockhardnessmap);
}
else{

  printf("Loading Dungeon\n");
  int testMax_Rooms = loadDungeon(filename, dungeonmap, rockhardnessmap,Rooms);
  if(testMax_Rooms == -1){
    testMax_Rooms = Max_Rooms;
    printf("Creating Dungeon\n");
    Max_Rooms = createDungeon(dungeonmap,testMax_Rooms,Rooms,rockhardnessmap);
  }
  else{
  
    Max_Rooms = testMax_Rooms;
  }  
}

if(save){
  printf("Saving Dungeon\n");  
  saveDungeon(filename, rockhardnessmap,Rooms, Max_Rooms);    
}

  int cRoom = rand() % Max_Rooms;
  int x = Rooms[cRoom].x;
  int y = Rooms[cRoom].y;
  int w = Rooms[cRoom].w;
  int h = Rooms[cRoom].h;
  bool inRoom = false;
  
  if(!player_set){
    p.x = x + rand() % w;
    p.y = y + rand() % h;
  }
  else{
	if(rockhardnessmap[p.x][p.y] == 0){
	printf("located in room\n");
	inRoom = true;
	}
  }
  
  
  
  if(!inRoom){
    if(player_set){
    printf("-p %d %d arg is not in a room.\n", p.x, p.y);
    player_set = false;
    }
  }
  
  if(!player_set){
    p.x = x + rand() % w;
    p.y = y + rand() % h;
  }
  
  
  
  int TunnelMap[MAX_H][MAX_V];
  int NoneTunnelMap[MAX_H][MAX_V];
  int PNTMap[MAX_H*MAX_V];
  int PTMap[MAX_H*MAX_V];
  int PDestMap[MAX_H*MAX_V];
  memset(PNTMap,0,sizeof(PNTMap));
  memset(PTMap,0,sizeof(PTMap));
  memset(PDestMap,0,sizeof(PDestMap));
  monster_list_t* mList;
  mList = malloc(sizeof(monster_list_t));
  monsters = malloc(sizeof(monster_node_t)*nummon);
  queue_init(mList);
  
  initMonster(p, mList, monsters,&nummon,Rooms,Max_Rooms);
  
  
  printf("Generating %d Monsters.\n",nummon);
  //dungeonmap[p.x][p.y] = '@';
  //printDungeon(dungeonmap);
  
  
  createNoTunnelMap(p, rockhardnessmap,NoneTunnelMap,PNTMap);
  //printNoTunnelMap(p,rockhardnessmap, NoneTunnelMap);
  
  createTunnelMap(p, rockhardnessmap,TunnelMap,PTMap);
  //printTunnelMap(p,TunnelMap);

  free(filename);
  
  
  initscr();
  
  start_color();
  
  init_pair(1, COLOR_BLACK, COLOR_YELLOW);//Dungeon background
  
  init_pair(2, COLOR_RED,COLOR_BLACK); //Erratic
  init_pair(3, COLOR_YELLOW,COLOR_BLACK); 
  init_pair(4, COLOR_GREEN,COLOR_BLACK);
  init_pair(5, COLOR_BLUE,COLOR_BLACK); //Intelligent
  init_pair(6, COLOR_CYAN,COLOR_BLACK);
  init_pair(7, COLOR_MAGENTA,COLOR_BLACK);

  init_pair(12, COLOR_RED,COLOR_YELLOW); //Erratic
  init_pair(13, COLOR_YELLOW,COLOR_YELLOW); 
  init_pair(14, COLOR_GREEN,COLOR_YELLOW);
  init_pair(15, COLOR_BLUE,COLOR_YELLOW); //Intelligent
  init_pair(16, COLOR_CYAN,COLOR_YELLOW);
  init_pair(17, COLOR_MAGENTA,COLOR_YELLOW);



  
  init_pair(8, COLOR_WHITE,COLOR_RED);
  
  //timeout(0);
  
  int key = 0;
  //int stuck;
  
  p.speed = p_speed;
  character events[5000];
  
  events[0].id = 0;
  events[0].eventTime = 0;//Player starts first
  events[0].player = true;
  p._isAlive = true;
  
  events[0].timeCreated = time(&ti.tv_sec);
  events[0].timeCreatedNano = ti.tv_nsec;
  int eventSize = 0;
  int eventPos[5000];
  cPush(events,events[0],&eventSize,eventPos);
  
  for(i = 1;i<=(nummon); i++){
    events[i].id = i;
    events[i].player = false;
    events[i].eventTime = 1000/monsters[i-1]->data.speed;
    monsters[i-1]->data._isAlive = true;
    events[i].mNode = monsters[i-1];
    events[i].timeCreated = time(&ti.tv_sec);
    events[i].timeCreatedNano = ti.tv_nsec;
    cPush(events,events[i],&eventSize,eventPos);
  }
  
  noecho();
  bool gameover = false;
  curs_set(0);
  int dest_room = 0;

  player p_dest;
  p_dest.x = Rooms[dest_room].x;
  p_dest.y = Rooms[dest_room].y;
  int dummyTunnel[MAX_H][MAX_V];
  createTunnelMap(p_dest, rockhardnessmap,dummyTunnel,PDestMap);
  
  while(key!='q' && !gameover){
    
    inCursesPrintDungeon(dungeonmap);
    attron(A_BLINK);
    mvprintw(p.y,p.x,"@");
    attroff(A_BLINK);
    printMonsters(mList,nummon);
    
    //Event Queue
    cHeap(events,eventSize,0,eventPos);
    character t = cPop(events,&eventSize,eventPos);
    
    for(i = 0;i<eventSize;i++){
	events[i].eventTime = events[i].eventTime - t.eventTime < 0 ? 0 :events[i].eventTime - t.eventTime  ;  
    }
    
     //Can Monsters see Player
    bool gcheck = true;
    monster_node_t *m_itr = mList->head;
    while(m_itr){
      
      checkLos(&(m_itr->data), p,rockhardnessmap);
      if(m_itr->data._isAlive){
	gcheck = false; 
      }
      m_itr=m_itr->next;
    }

    gameover = gcheck;
    
    

    int new_x = 0;
    int new_y = 0;
    int pdx = 0;
    int pdy = 0; 
    
   if(t.player == true){
    flushinp();
    key = 0;
    
    
    if(!player_auto){
      while(!validKeyCheck(key) || (new_x<1 || new_x>(MAX_H-2) || new_y< 1 || new_y >(MAX_V-2))){
	move(21+1,0);
	printw("Please Select Action for Player. Press 5 to wait or press q to quit.\n");
	printw("7  8  9\n %c | / \n4--5--6\n / | %c \n1  2  3\n", 92 , 92);
	key = getch();
	pdx = 0;
	pdy = 0;
	getnewDirection(translateKeyinput(key),&pdx,&pdy);
	
	new_x = p.x+pdx;
	new_y = p.y+pdy;
      }
    }
    else{
      while((new_x<1 || new_x>(MAX_H-2) || new_y< 1 || new_y >(MAX_V-2))){
	
	if(Rooms[dest_room].x  == p.x && Rooms[dest_room].y  == p.y){
	  dest_room = (dest_room +1) % Max_Rooms;
	  player p_dest;
	  p_dest.x = Rooms[dest_room].x;
	  p_dest.y = Rooms[dest_room].y;
	  createTunnelMap(p_dest, rockhardnessmap,dummyTunnel,PDestMap);
	  
	}
	
	if(!rand()%2){
	key = rand() % 8 + '0';
	pdx = 0;
	pdy = 0;
	getnewDirection(translateKeyinput(key),&pdx,&pdy);
	
	new_x = p.x+pdx;
	new_y = p.y+pdy;
	}
	else{
	  move(27,0);
	  
	  printw("%d : %d", PDestMap[p.x+p.y*MAX_H]%MAX_H, PDestMap[p.x+p.y*MAX_H]/MAX_H);
	  new_x = PDestMap[p.x+p.y*MAX_H]%MAX_H;
	  new_y = PDestMap[p.x+p.y*MAX_H]/MAX_H;
	  pdx = new_x - p.x;
	  pdy = new_y - p.y;
	  
	  
	}
	
      }
    }
    
    if(key == '5'){
    }
    else{
      //Translate Key  
      if(!player_auto){
      pdx = 0;
      pdy = 0;
      getnewDirection(translateKeyinput(key),&pdx,&pdy);
      
	new_x = p.x+pdx;
	new_y = p.y+pdy;
      }
      if(rockhardnessmap[p.x+pdx][p.y+pdy] == 0){
	p.x=new_x;
	p.y=new_y;
	createTunnelMap(p, rockhardnessmap,TunnelMap,PTMap);
	createNoTunnelMap(p, rockhardnessmap,NoneTunnelMap,PNTMap);
	    if(verbose_flag){
	      move(21+1,0);
	      printw("Moved Player to x:%d y:%d with speed of %d.\n",p.x,p.y, p_speed);
      
	    }
      }
      else{
	    rockhardnessmap[new_x][new_y]-=85;
	    if(rockhardnessmap[new_x][new_y] <= 0){
	      rockhardnessmap[new_x][new_y] = rockhardnessmap[new_x][new_y] < 0 ? 0:rockhardnessmap[new_x][new_y];
	      dungeonmap[new_x][new_y] = '#';
	      createTunnelMap(p, rockhardnessmap,TunnelMap,PTMap);
	      createNoTunnelMap(p, rockhardnessmap,NoneTunnelMap,PNTMap);  
	      p.x = new_x;
	      p.y = new_y;
	      if(verbose_flag){
		move(21+1,0);
		printw("Tunneled and Moved Player to x:%d y:%d.\n",p.x,p.y);
      
	      }
	    }
	    else{
	      if(verbose_flag){
		move(21+1,0);
		printw("Player Attempted to Tunnel to x:%d y:%d.\n",p.x,p.y);
	      }
	      
	    }
      }
      
      
      m_itr = mList->head;
      while(m_itr){
	if(p.x == m_itr->data.x && p.y == m_itr->data.y){
	  nummon--;
	  m_itr->data._isAlive = false;
	}
	m_itr=m_itr->next;
      }
    }
    

    t.eventTime = 1000/p.speed;
    clock_gettime(CLOCK_REALTIME,&ti);
    t.timeCreated = time(&ti.tv_sec);
    t.timeCreatedNano = ti.tv_nsec;
    cPush(events,t,&eventSize,eventPos);     
  }
   else{
     monster cur_monster = t.mNode->data;
     
     if(cur_monster._isAlive){
     //Handle Monster Behaviour 
     new_x = cur_monster.x;
     new_y = cur_monster.y;
     
     if(!(cur_monster.stats.attr & 0x01) && !cur_monster._seen){
      cur_monster.dest_x = new_x;
      cur_monster.dest_y = new_y; 
     }
     
     monsterMovement(p,&cur_monster,PNTMap,PTMap,rockhardnessmap,&new_x,&new_y);
     
     if(rockhardnessmap[new_x][new_y] == 0){
	bool check = false;
	//int i;
	m_itr = mList->head;
	while(m_itr){
	  if(m_itr->data._isAlive && new_x==m_itr->data.x && new_y==m_itr->data.y ){
	    check=true;
	  }
	  m_itr = m_itr->next;
	}
       
      
      if(cur_monster.x == cur_monster.dest_x &&
	cur_monster.y == cur_monster.dest_y){
	cur_monster._seen = false;  
      }
      
       //Move Monster
      if(!check){
	cur_monster.x = new_x;
	cur_monster.y = new_y;
	if(verbose_flag){     
	  move(21+1,0);
	  printw("%1x Monster moved to x: %d y: %d with speed of %d\n",
		cur_monster.stats.attr,
		cur_monster.x,
		cur_monster.y,
		cur_monster.speed
		);
	  
	}
	
	
      }
      else{
      	if(verbose_flag){     
	  move(21+1,0);
	  printw("%1x Monster stayed at x: %d y: %d with speed of %d\n",
		cur_monster.stats.attr,
		cur_monster.x,
		cur_monster.y,
		cur_monster.speed
		);
	  
	}
	
      }
      
      //Game Over Condition Flag Check
      
      if(new_x == p.x && new_y==p.y){
	gameover = true;
	p._isAlive = false;
      }
       
    }
     else{
      if((cur_monster.stats.attr>>2) & 0x01){
	  rockhardnessmap[new_x][new_y]-=85;
	  if(rockhardnessmap[new_x][new_y] <= 0){
	    rockhardnessmap[new_x][new_y] = rockhardnessmap[new_x][new_y] < 0 ? 0:rockhardnessmap[new_x][new_y];
	    dungeonmap[new_x][new_y] = '#';
	    createTunnelMap(p, rockhardnessmap,TunnelMap,PTMap);
	    createNoTunnelMap(p, rockhardnessmap,NoneTunnelMap,PNTMap);
	    cur_monster.x = new_x;
	    cur_monster.y = new_y;
	    
	    if(verbose_flag){     
	      move(21+1,0);
	      printw("%1x Monster Tunneled and Moved to x: %d y: %d with speed of %d\n",
		    cur_monster.stats.attr,
		    cur_monster.x,
		    cur_monster.y,
		    cur_monster.speed
		    );
	      
	    }
	    
	  }
	  else{
	    if(verbose_flag){     
	      move(21+1,0);
	      printw("%1x Monster Attempting to Tunnel to x: %d y: %d with speed of %d\n",
		    cur_monster.stats.attr,
		    new_x,
		    new_y,
		    cur_monster.speed
		    );  
	    }
	    
	  }
	}
	else{
      	if(verbose_flag){     
	  move(21+1,0);
	  printw("%1x Monster stayed at x: %d y: %d with speed of %d\n",
		cur_monster.stats.attr,
		cur_monster.x,
		cur_monster.y,
		cur_monster.speed
		);
	  
	}
	  
	}
	
     }
     
     cur_monster.inRoom = dungeonmap[cur_monster.x][cur_monster.y] == '.' ? true : false;
     
       
    }
      t.mNode->data = cur_monster;
     t.eventTime = 1000/cur_monster.speed;
     clock_gettime(CLOCK_REALTIME,&ti);    
     t.timeCreated = time(&ti.tv_sec);
     t.timeCreatedNano = ti.tv_nsec;
     cPush(events,t,&eventSize,eventPos);
   }   
    
    move(21+1,0);// Move to End of the line
    
    if(t.player ==true){
      //key = getch();     
    }
    
    refresh();
    if(!verbose_flag){
      if(t.eventTime> 0){ 
	usleep(t.eventTime*100);
      }
    }
    else{
      usleep(1000000);
    }
    
    clear();
    if(player_auto){
    timeout(0);
      key = getch();
    }
  }
  
  move(21+1,0);// Move to End of the line
  
    refresh();
  //printDungeon(dungeonmap);
    inCursesPrintDungeon(dungeonmap);
    attron(A_BLINK);
    mvprintw(p.y,p.x,"@");
    attroff(A_BLINK);
    printMonsters(mList,nummon);
  
    
    move(21+1,0);
    key = 0;
    timeout(-1);
  while(key!='q'){
    move(21+1,0);
    if(p._isAlive){
      if(nummon > 0){
	printw("You Win! Unless you rage quit. Which I'm sure you did, %d enemies left.\n", nummon);
      }
      else{
	printw("You Win! PETA is going to have your head.\n");
      }
    }
    else{
      printw("You Died, Noob.\n");
    }
    
    printw("Press q to quit\n");
    key = getch();
    inCursesPrintDungeon(dungeonmap);
    attron(COLOR_PAIR(8));
    mvaddch(p.y,p.x,'@');
    attroff(COLOR_PAIR(8));
    printMonsters(mList,nummon);
    
  }
  
  endwin();
  
  queue_delete(mList);
  
  free(mList);
  free(monsters);
  
return 0;
}