#include "adjacencylist.h"

//Utility Function to Add Edges to our node tree
node_t * addEdge( node_t * start, int end, int weight){

//Initialize Memory Space of Next Node to zero
node_t * Node = (node_t *) malloc(sizeof(node_t));

//Set Parameters
Node->vertex = end;
Node->weight = weight;

Node->next=start;
return Node;
}

int edgeQueue_init(edgeQueue_t *e){
  e->size = 0;
  e->head = e->tail = NULL;
  return 0;
}

int edgeQueue_delete(edgeQueue_t *e){
  int i,j;
  while(!edgeQueue_remove(e,&i,&j))
   ;
  
  return 0;
  
}
int edgeQueue_add(edgeQueue_t *e, int end, int weight){
  if(e->head){
    e->tail->next = malloc(sizeof (*e->tail->next));
    e->tail = e->tail->next;
    e->tail->vertex = end;
    e->tail->weight = weight;
    e->tail->next = NULL;
    e->size++;
    
  }
  else{
    e->tail = e->head = malloc(sizeof (*e->tail->next));
    e->tail->vertex = end;
    e->tail->weight = weight;
    e->tail->next = NULL;
    e->size++;
    
  }
  return 0;
}
int edgeQueue_remove(edgeQueue_t *e,int *vertex, int *weight){
    node_t *n;
    if(!e->size){
      return 1;
    }
    n=e->head;
    e->head = e->head->next;
    e->size--;
    *vertex = n->vertex;
    *weight = n->weight;
    free(n);
    if(!e->size){
      e->tail = NULL;
    }
  return 0;
}
int edgeQueue_size(edgeQueue_t *e){

  return e->size;
}



int left(int i){

  return (2*i+1);
}

int right(int i){

  return (2*i+2);
}

int parent(int i){
  return (i-1)/2;
}


//organize a heap containing our cumulative distance
//traveled down nodes
void heap(vertex_t Heap[], int size, int check, int pos[]){
	
	//Temp Varible for swapping.
	vertex_t temp;
	
	int l = left(check);
	int r = right(check);
	int index = check;
	
	if(l<size && Heap[l].dist < Heap[check].dist){
	  index = l;
	}
	if(r<size && Heap[r].dist < Heap[index].dist){
	
	  index = r;
	}
	
	if(index!=check){
	  
	  
	  pos[Heap[check].vertex] = index;
	  pos[Heap[index].vertex] = check;
	  
	  temp = Heap[check];
	  Heap[check]=Heap[index];
	  Heap[index] = temp;
	  heap(Heap,size,index,pos);
	  
	}

}


//decrease key command. Rebuilds the heap and pushes the
//smallest value to the top of the stack.
void decreaseKey(vertex_t Heap[], vertex_t Node, int pos[]){
	
	int i = pos[Node.vertex];
	vertex_t temp;

	Heap[i].dist = Node.dist;
	
	while(i!=0 && Heap[parent(i)].dist > Heap[i].dist){
	      pos[Heap[i].vertex] = parent(i);
	      pos[Heap[parent(i)].vertex] = i;
	      
	      temp = Heap[parent(i)];
	      Heap[parent(i)] = Heap[i];
	      Heap[i] = temp;
	      i = parent(i);
	
	  
	}
}
//Function to get the smallest value
//With the heap, should be at the head of the heap.
vertex_t pop(vertex_t Heap[], int size, int pos[]){
	pos[Heap[0].vertex] = size-1;
	pos[Heap[size-1].vertex] = 0;
	
	vertex_t min=Heap[0];
	Heap[0] = Heap[size-1];
	--size;
	heap(Heap,size,0,pos);
	return min;
}


//Construct the heap.
void buildHeap(vertex_t Heap[], int size, int pos[]){
	int i;
	
	for(i = size;i>0;--i){
		heap(Heap,size,i,pos);
	}

}