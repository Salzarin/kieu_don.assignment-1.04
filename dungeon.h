#ifndef DUNGEON_H
#define DUNGEON_H

#include "characterlist.h"
#include "dijkstras.h"
#include "monster.h"


#include <unistd.h>
#include <ncurses.h>
#include <errno.h>
#include <sys/stat.h>



enum direction{
  LEFT_DOWN,
  DOWN,
  RIGHT_DOWN,
  RIGHT,
  RIGHT_UP,
  UP,
  LEFT_UP,
  LEFT
};





typedef struct playerdata{
  int x;
  int y;
  int speed;
  int direction;
  bool _isAlive;
} player;





typedef union _data{
  char str[4];
  int integer;
} data;


typedef struct roomdata{
	//X and Y are position of top left of the room.
	int x; 
	int y;
	int w;
	int h;
	int cx;
	int cy;
	bool valid;
} room;


int getnewDirection(int d, int *x,int *y);
int getnewRandDirection(int d, int *x,int *y);


int translateKeyinput(int key);
bool validKeyCheck(int key);
bool checkRoomIntersection(room A, room B);
int createDungeon(int dungeonmap[MAX_H][MAX_V], int Max_Rooms, room Room[], int rockhardnessmap[MAX_H][MAX_V]);
int saveDungeon(char * filename, int rock_hard_map[MAX_H][MAX_V], room Rooms[], int Max_Rooms);
int loadDungeon(char * filename, int dungeonmap[MAX_H][MAX_V], int rock_hard_map[MAX_H][MAX_V], room Rooms[]);
data endianSwap(data e);
void initializeDungeon(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V], bool hardness_set);
void printDungeon(int dungeonmap[MAX_H][MAX_V]);
void drawRooms(int dungeonmap[MAX_H][MAX_V],int hardnessmap[MAX_H][MAX_V], room Rooms[], int Max_Rooms);
void drawCorridors(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V], room Room[], int Max_Rooms);
void drawCorridorsHardness(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V]);
void printHardnessMap(int rockhardnessmap[MAX_H][MAX_V]);
void loadCorridors(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V]);

//Monster.c 
void createTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V], int PTMap[MAX_H*MAX_V]);
void createNoTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V],int PNTMap[MAX_H*MAX_V]);
void initMonster(player p, monster_list_t * e, monster_node_t ** monsters, int *nummon, room Rooms[1000],int numrooms);
void printMonsters(monster_list_t *monsters, int nummon);
void monsterMovement(player p, monster *mon, int PNTmap[MAX_H*MAX_V], int PTmap[MAX_H*MAX_V], int Hmap[MAX_H][MAX_V], int * new_x, int *new_y);
bool checkLos(monster *mon, player p, int map[MAX_H][MAX_V]);

void printNoTunnelMap(player p,int hardmap[MAX_H][MAX_V], int Tmap[MAX_H][MAX_V]);
void printTunnelMap(player p, int Tmap[MAX_H][MAX_V]);
void printColor(int i);
void inCursesPrintDungeon(int dungeonmap[MAX_H][MAX_V]);

int movePlayer(player *p, int dx, int dy,int hardmap[MAX_H][MAX_V],int dungeonmap[MAX_H][MAX_V]);





#endif